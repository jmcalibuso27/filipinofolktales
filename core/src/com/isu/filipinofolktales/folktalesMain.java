package com.isu.filipinofolktales;

import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AppScreens.LoadingScreen;

public class folktalesMain extends AbstractGame {

	@Override
	public void create () {
		setScreen(new LoadingScreen(this));
	}

}
