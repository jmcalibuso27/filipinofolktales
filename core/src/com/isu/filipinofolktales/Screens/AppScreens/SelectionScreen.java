package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.AudioManager;
import com.isu.filipinofolktales.Util.DataHolder;

public class SelectionScreen extends AbstractScreen {

    public SelectionScreen(AbstractGame game) {
        super(game);
        skin = Assets.instance.skins.stage;
        createStage();
    }

    private void createStage(){
        Image bg = new Image(skin, "start_bg");
        bg.setFillParent(true);
        stage.addActor(bg);
        createButtons();
    }

    private void createButtons(){
        Button btnAlamat = new Button(skin, "btnAlamat");
        btnAlamat.setPosition(0, 0);
        btnAlamat.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                StoryScreen storyScreen = new StoryScreen(game, DataHolder.data.get("alamat"));
                game.setScreen(storyScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Button btnEpiko = new Button(skin, "btnEpiko");
        btnEpiko.setPosition(0, 0);
        btnEpiko.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                StoryScreen storyScreen = new StoryScreen(game, DataHolder.data.get("epiko"));
                game.setScreen(storyScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Button btnKwento = new Button(skin, "btnKwento");
        btnKwento.setPosition(0, 0);
        btnKwento.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                StoryScreen storyScreen = new StoryScreen(game, DataHolder.data.get("kwento"));
                game.setScreen(storyScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Button btnPabula = new Button(skin, "btnPabula");
        btnPabula.setPosition(0, 0);
        btnPabula.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                StoryScreen storyScreen = new StoryScreen(game, DataHolder.data.get("pabula"));
                game.setScreen(storyScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Button btnParabula = new Button(skin, "btnParabula");
        btnParabula.setPosition(0, 0);
        btnParabula.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                StoryScreen storyScreen = new StoryScreen(game, DataHolder.data.get("parabula"));
                game.setScreen(storyScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Table tbl = new Table();
        tbl.setSize(720, 1000);
        tbl.setY(220);

        tbl.row();
        tbl.add(btnAlamat).center().padTop(30);
        tbl.row();
        tbl.add(btnEpiko).center().padTop(30);
        tbl.row();
        tbl.add(btnKwento).center().padTop(30);
        tbl.row();
        tbl.add(btnPabula).center().padTop(30);
        tbl.row();
        tbl.add(btnParabula).center().padTop(30);

        Button btnHome = new Button(skin, "btnHome");
        btnHome.setPosition(291, 150);
        btnHome.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, true, Interpolation.smooth));
            }
        });

        stage.addActor(btnHome);
        stage.addActor(tbl);
    }

    @Override
    public void render(float deltaTime) {
        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            game.setScreen(new MainMenuScreen(game), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, false, Interpolation.smooth));
        }
        Gdx.gl.glClearColor(1 ,1 ,1 ,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {}

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }
}
