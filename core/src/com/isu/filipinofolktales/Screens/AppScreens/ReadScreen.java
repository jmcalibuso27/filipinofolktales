package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.DataHolder;
import com.isu.filipinofolktales.Util.DataHolder.Story;

public class ReadScreen extends AbstractScreen implements InputProcessor {

    private Array<Image> images;
    private int current;
    private Array<Story> parentStories;
    private float downX;
    private String atlasName;

    public ReadScreen(AbstractGame game, Array<Story> parentStories, String atlasName, int count) {
        super(game);
        this.parentStories = parentStories;
        skin = Assets.instance.skins.stage;
        this.atlasName = atlasName;
        createStage(atlasName, count);
        current = 0;
        for(DataHolder.Question q : DataHolder.data.getQuestions(atlasName)){
            Gdx.app.log("", "Question : " + q.getQuestion());
            for(DataHolder.Choice c : q.getChoices()){
                Gdx.app.log("", "Choice : " + c.getChoice() + c.isCorrect());
            }
            Gdx.app.log("", "---------------------");
        }
    }

    private void createStage(String atlasName, int count){
        images = new Array<Image>();
        for(int i = 1; i <= count; i++){
            Image img = new Image(skin, atlasName + i);
            img.setFillParent(true);
            stage.addActor(img);
            images.add(img);

            if(i > 1){
                img.setX(720);
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            game.setScreen(new StoryScreen(game, parentStories), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, false, Interpolation.smooth));
        }

        Gdx.gl.glClearColor(1 ,1 ,1 ,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {}

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return this;
    }

    @Override
    public boolean keyDown(int keycode) {return false;}

    @Override
    public boolean keyUp(int keycode) {return false;}

    @Override
    public boolean keyTyped(char character) {return false;}

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        downX = screenX;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(downX > screenX && Math.abs(downX - screenX) >= 20){
            if(current + 1 < images.size){
                current += 1;
                images.get(current).addAction(Actions.parallel(Actions.moveTo(0, 0, 0.4f, Interpolation.smooth)));

            } else if(current + 1 == images.size){
                game.setScreen(new QuizScreen(game, atlasName, parentStories), ScreenTransitionSlide.init(0.4f, ScreenTransitionSlide.LEFT, true, Interpolation.smooth));
            }
        } else if(downX < screenX && Math.abs(downX - screenX) >= 20){
            if(current - 1 >= 0){
                images.get(current).addAction(Actions.parallel(Actions.moveTo(720, 0, 0.4f, Interpolation.smooth)));
                current -= 1;
            } else if(current - 1 < 0){
                game.setScreen(new StoryScreen(game, parentStories), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, true, Interpolation.smooth));
            }
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
