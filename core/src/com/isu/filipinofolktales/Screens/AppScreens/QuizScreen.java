package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.AudioManager;
import com.isu.filipinofolktales.Util.DataHolder;
import com.isu.filipinofolktales.Util.DataHolder.Question;
import com.isu.filipinofolktales.Util.DataHolder.Choice;

public class QuizScreen extends AbstractScreen {

    private Array<Question> questions;
    private Array<DataHolder.Story> parentStories;
    private Skin lblSkin;
    private Label lblQuestion;
    private Table btnGroup;
    private Array<Button> buttons;
    private int current;
    private int correct;
    private int score;
    private float delay;
    private boolean isDone;
    private Group scoreBoard;
    private Image check, wrong;

    public QuizScreen(AbstractGame game, String atlasName, Array<DataHolder.Story> parentStories) {
        super(game);
        this.parentStories = parentStories;
        questions = DataHolder.data.getQuestions(atlasName);
        skin = Assets.instance.skins.stage;
        lblSkin = Assets.instance.skins.norotate;
        current = 0;
        score = 0;
        delay = 0f;
        isDone = false;
        createStage(questions.get(current));
        loadChoices(questions.get(current));
    }

    private void createStage(Question q){
        Image bg = new Image(skin, "quizbg");
        bg.setFillParent(true);
        stage.addActor(bg);

        lblQuestion = new Label("", lblSkin, "big");
        lblQuestion.setWrap(true);
        lblQuestion.setAlignment(Align.center);
        lblQuestion.setSize(720, 480);
        lblQuestion.setPosition(0, 800);

        stage.addActor(lblQuestion);

        btnGroup = new Table();
        btnGroup.setSize(720, 800);
        btnGroup.setPosition(0, 0);

        stage.addActor(btnGroup);

        check = new Image(skin, "correct");
        check.setPosition(200, 1280); // 517
        check.setUserObject(new Vector2(200, 517));

        wrong = new Image(skin, "wrong");
        wrong.setPosition(235, 1280); // 514
        wrong.setUserObject(new Vector2(235, 514));

        stage.addActor(check);
        stage.addActor(wrong);

        scoreBoard = new Group();
        scoreBoard.setSize(449, 255);
        scoreBoard.setOrigin(Align.center);
        scoreBoard.setPosition(135, 752);
        scoreBoard.setVisible(false);

        Image scoreBg = new Image(skin, "score");
        scoreBg.setFillParent(true);

        scoreBoard.addActor(scoreBg);

        stage.addActor(scoreBoard);
    }

    private void loadChoices(Question question){
        btnGroup.clearChildren();
        lblQuestion.setText(question.getQuestion());
        for(int i = 0; i < question.getChoices().size; i++){
            Choice choice = question.getChoices().get(i);
            final int num = i;
            TextButton c = new TextButton(choice.getChoice(), lblSkin, "choiceBtn");
            c.getLabelCell().pad(10);
            c.getLabel().setWrap(true);
            c.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    if(!check.hasActions() && !wrong.hasActions()){
                        AudioManager.audio.playClick();
                        answer(num);
                    }
                }
            });

            btnGroup.row();
            btnGroup.add(c).expandX().uniformY();

            if(choice.isCorrect()){
                correct = i;
            }
        }
    }

    private void showAnswer(Image img){
        Vector2 v = (Vector2) img.getUserObject();
        img.addAction(Actions.sequence(Actions.moveTo(v.x, v.y, 0.3f, Interpolation.bounceIn), Actions.delay(0.6f), Actions.moveTo(v.x, -250, 0.3f, Interpolation.smooth), Actions.moveTo(v.x, 1280)));
    }

    private void answer(int num){
        if(num == correct) {
            score++;
            showAnswer(check);
        } else {
            showAnswer(wrong);
        }
        current++;
        if(current >= questions.size){
            isDone = true;

            Label lblScore = new Label(score + "", lblSkin, "big");
            lblScore.setSize(449, 200);
            lblScore.setAlignment(Align.center);
            lblScore.setPosition(0, 10);
            lblScore.setFontScale(1.5f);
            scoreBoard.addActor(lblScore);

            scoreBoard.setVisible(true);
            scoreBoard.addAction(Actions.sequence(Actions.scaleTo(1.3f, 1.3f, 0.4f), Actions.scaleTo(0.8f, 0.8f, 0.49f)));
        } else {
            loadChoices(questions.get(current));
        }
    }

    @Override
    public void render(float deltaTime) {
        if(Gdx.input.isKeyPressed(Input.Keys.BACK)){
            game.setScreen(new StoryScreen(game, parentStories), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, false, Interpolation.smooth));
        }

        Gdx.gl.glClearColor(1 ,1 ,1 ,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();

        if(isDone){
            delay+=deltaTime;
            if(delay >= 4){
                isDone = false;
                game.setScreen(new SelectionScreen(game), ScreenTransitionSlide.init(0.4f, ScreenTransitionSlide.LEFT, true, Interpolation.smooth));
            }
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }
}
