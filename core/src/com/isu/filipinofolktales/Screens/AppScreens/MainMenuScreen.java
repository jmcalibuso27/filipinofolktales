package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.AudioManager;
import com.isu.filipinofolktales.Util.GameSettings;

public class MainMenuScreen extends AbstractScreen {

    private Group developers, settings;

    public MainMenuScreen(AbstractGame game) {
        super(game);
        skin = Assets.instance.skins.stage;
        createStage();
    }

    private void createStage(){
        Image bg = new Image(skin, "mainmenu");
        bg.setFillParent(true);
        stage.addActor(bg);
        createButtons();
        createWindows();
    }

    private void createButtons(){
        Button btnStart = new Button(skin, "start");
        btnStart.setPosition(134, 563);
        btnStart.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                game.setScreen(new SelectionScreen(game), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
            }
        });

        Button btnDevelopers = new Button(skin, "developers");
        btnDevelopers.setPosition(134, 421);
        btnDevelopers.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                developers.addAction(Actions.parallel(Actions.moveTo(0, 0, 0.5f, Interpolation.smooth), Actions.fadeIn(0.4f)));
            }
        });

        Button btnSettings = new Button(skin, "settings");
        btnSettings.setPosition(134, 277);
        btnSettings.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                settings.addAction(Actions.parallel(Actions.moveTo(0, 0, 0.5f, Interpolation.smooth), Actions.fadeIn(0.4f)));
            }
        });

        Button btnExit = new Button(skin, "exit");
        btnExit.setPosition(134, 138);
        btnExit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        stage.addActor(btnStart);
        stage.addActor(btnDevelopers);
        stage.addActor(btnSettings);
        stage.addActor(btnExit);
    }

    private void createWindows(){
        developers = new Group();
        developers.setSize(720, 1280);

        Image bgDevelopers = new Image(skin, "developers");
        bgDevelopers.setFillParent(true);

        Button btnDeveloperHome = new Button(skin, "btnHome");
        btnDeveloperHome.setPosition(291, 150);
        btnDeveloperHome.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                developers.addAction(Actions.parallel(Actions.moveTo(720, 0, 0.5f, Interpolation.smooth), Actions.fadeOut(0.6f)));
            }
        });

        developers.addActor(bgDevelopers);
        developers.addActor(btnDeveloperHome);
        developers.setPosition(720, 0);
        developers.getColor().a = 0f;


        settings = new Group();
        settings.setSize(720, 1280);

        Image bgSettings = new Image(skin, "settings");
        bgSettings.setFillParent(true);

        final Slider volume = new Slider(0 , 1, 0.1f, true, skin, "default");
        volume.setPosition(297, 428);
        volume.setSize(126, 523);
        volume.setValue(GameSettings.settings.getVolume());
        volume.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GameSettings.settings.setVolume(volume.getValue());
                AudioManager.audio.updateVolume();
            }
        });

        Button btnSettingsHome = new Button(skin, "btnHome");
        btnSettingsHome.setPosition(291, 150);
        btnSettingsHome.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                AudioManager.audio.playClick();
                settings.addAction(Actions.parallel(Actions.moveTo(720, 0, 0.5f, Interpolation.smooth), Actions.fadeOut(0.6f)));
            }
        });

        settings.addActor(bgSettings);
        settings.addActor(volume);
        settings.addActor(btnSettingsHome);
        settings.setPosition(720, 0);
        settings.getColor().a = 0f;

        stage.addActor(developers);
        stage.addActor(settings);
    }

    @Override
    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1 ,1 ,1 ,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return stage;
    }
}
