package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.AudioManager;
import com.isu.filipinofolktales.Util.DataHolder;

public class StoryScreen extends AbstractScreen implements InputProcessor{

    private Array<DataHolder.Story> stories;
    private InputMultiplexer multiplexer;
    private float downX;

    public StoryScreen(AbstractGame game, Array<DataHolder.Story> stories) {
        super(game);
        this.stories = stories;
        skin = Assets.instance.skins.stage;
        createStage();

        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(this);
        multiplexer.addProcessor(stage);
    }

    private void createStage(){
        Image bg = new Image(skin, "start_bg");
        bg.setFillParent(true);
        stage.addActor(bg);

        createButtons();
    }

    private void createButtons(){
        Table tbl = new Table();
        tbl.setSize(720, 1000);
        tbl.setY(100);

        for(int i = 0; i < stories.size; i++){
            final DataHolder.Story story = stories.get(i);
            TextButton button = new TextButton(story.getTitle(), Assets.instance.skins.norotate, "default");
            button.getLabel().setWrap(true);
            button.getLabel().setFontScale(1.3f);
            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    AudioManager.audio.playClick();
                    ReadScreen readScreen = new ReadScreen(game, stories, story.getAtlas(), story.getCount());
                    game.setScreen(readScreen, ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, false, Interpolation.smooth));
                }
            });

            tbl.row();
            tbl.add(button).center().padTop(30);
        }

        stage.addActor(tbl);
    }

    @Override
    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1 ,1 ,1 ,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {}

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return multiplexer;
    }

    @Override
    public boolean keyDown(int keycode) {return false;}

    @Override
    public boolean keyUp(int keycode) {return false;}

    @Override
    public boolean keyTyped(char character) {return false;}

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        downX = screenX;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(downX < screenX && Math.abs(downX - screenX) >= 20){
            game.setScreen(new SelectionScreen(game), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.RIGHT, true, Interpolation.smooth));
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {return false;}

    @Override
    public boolean mouseMoved(int screenX, int screenY) {return false;}

    @Override
    public boolean scrolled(int amount) {return false;}
}
