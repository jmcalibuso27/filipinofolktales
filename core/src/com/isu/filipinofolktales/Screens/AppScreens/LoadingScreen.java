package com.isu.filipinofolktales.Screens.AppScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.isu.filipinofolktales.Screens.AbstractGame;
import com.isu.filipinofolktales.Screens.AbstractScreen;
import com.isu.filipinofolktales.Screens.ScreenTransitionSlide;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.AudioManager;
import com.isu.filipinofolktales.Util.Constants;
import com.isu.filipinofolktales.Util.DataHolder;
import com.isu.filipinofolktales.Util.GameSettings;

public class LoadingScreen extends AbstractScreen {

    private boolean isLoading;

    public LoadingScreen(AbstractGame game) {
        super(game);
        Image img = new Image(new Texture(Gdx.files.internal(Constants.ATLAS_LOADING)));
        img.setFillParent(true);
        stage.addActor(img);
        isLoading = true;
    }

    @Override
    public void render(float deltaTime) {
        if(isLoading && Assets.instance.isLoading()){
            isLoading = false;
            Assets.instance.createClasses();
            game.setScreen(new MainMenuScreen(game), ScreenTransitionSlide.init(0.5f, ScreenTransitionSlide.LEFT, true, Interpolation.smooth));
            DataHolder.data.init();
            AudioManager.audio.playBGM();
        }

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void show() {
        Assets.instance.init(new AssetManager());
        GameSettings.settings.load();
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void pause() {}

    @Override
    public InputProcessor getInputProcessor() {
        return null;
    }
}
