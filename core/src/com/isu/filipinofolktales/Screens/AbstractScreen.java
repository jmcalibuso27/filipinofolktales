package com.isu.filipinofolktales.Screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.isu.filipinofolktales.Util.Assets;
import com.isu.filipinofolktales.Util.Constants;

public abstract class AbstractScreen implements Screen {

    protected AbstractGame game;
    protected Stage stage;
    protected Skin skin;

    public AbstractScreen(AbstractGame game) {
        this.game = game;
        stage = new Stage(new StretchViewport(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT));
    }

    public abstract void render(float deltaTime);

    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    public abstract void show();

    public abstract void hide();

    public abstract void pause();

    public void resume() {
        Assets.instance.init(new AssetManager());
    }

    public void dispose() {
        Assets.instance.dispose();
    }

    public abstract InputProcessor getInputProcessor();

}