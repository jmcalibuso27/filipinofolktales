package com.isu.filipinofolktales.Util;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface CommonInterface {

    void init();

    void update(float delta);

    void render(SpriteBatch batch);

    void dispose();
}