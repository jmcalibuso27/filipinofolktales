package com.isu.filipinofolktales.Util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameSettings {

    public final static GameSettings settings = new GameSettings();

    private Preferences preferences;
    private boolean sound;
    private float volume;

    private GameSettings() {
    }

    public void load() {
        preferences = Gdx.app.getPreferences("FilipinoFolktales.prefs");
        sound = preferences.getBoolean("sound", true);
        volume = preferences.getFloat("volume", 1.0f);
    }

    public boolean isSound() {
        return sound;
    }

    public void setSound(boolean sound) {
        this.sound = sound;
        preferences.putBoolean("sound", sound);
        preferences.flush();
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
        preferences.putFloat("volume", volume);
        preferences.flush();
    }
}