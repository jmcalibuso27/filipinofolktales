package com.isu.filipinofolktales.Util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

public class DataHolder {

    public static final DataHolder data = new DataHolder();

    private ArrayMap<String, Array<Story>> map;
    private XmlReader reader;
    private XmlReader.Element root;

    private DataHolder(){}

    public void init(){
        map = new ArrayMap<String, Array<Story>>();

        Array<Story> alamat = new Array<Story>();
        alamat.add(new Story("Ampalaya", "alamat_ampalaya", 3));
        alamat.add(new Story("Bulkang Mayon", "alamat_mayon", 4));
        alamat.add(new Story("Kasoy", "alamat_kasoy", 3));
        alamat.add(new Story("Langgam", "alamat_langgam", 3));
        alamat.add(new Story("Maria Makiling", "alamat_makiling", 9));

        map.put("alamat", alamat);

        Array<Story> epiko = new Array<Story>();
        epiko.add(new Story("Hudhud hi Aliguyon", "epiko_hudhud", 3));
        epiko.add(new Story("Ibalon", "epiko_ibalon", 3));

        map.put("epiko", epiko);

        Array<Story> kwento = new Array<Story>();
        kwento.add(new Story("Ang batik ng buwan", "kwento_batik", 2));
        kwento.add(new Story("Ang Diwata ng Karagatan", "kwento_diwata", 2));
        kwento.add(new Story("Ang Punong Kawayan", "kwento_kawayan", 2));
        kwento.add(new Story("Nakalbo ang Datu", "kwento_datu", 3));
        kwento.add(new Story("Si Mariang Mapangarapin", "kwento_maria", 2));

        map.put("kwento", kwento);

        Array<Story> pabula = new Array<Story>();
        pabula.add(new Story("Ang Magkapitbahay na Kambing at Kalabaw", "pabula_kambing_kalabaw", 3));
        pabula.add(new Story("Ang Matalinong Matsing at and Buwaya", "pabula_matsing_buwaya", 3));
        pabula.add(new Story("Ang Pagong at ang Kalabaw", "pabula_pagong_kalabaw", 3));
        pabula.add(new Story("Ang Unggoy at ang Pagong", "pabula_unggoy_pagong", 6));
        pabula.add(new Story("Bakit Laging Nag-aaway ang Aso, Pusa at Daga", "pabula_aso_pusa_daga", 3));

        map.put("pabula", pabula);

        Array<Story> parabula = new Array<Story>();
        parabula.add(new Story("Ang Manghuhula", "parabula_manghuhula", 4));
        parabula.add(new Story("Ang Pinaka Maliit na Bato", "parabula_bato", 2));
        parabula.add(new Story("Ang Tao at ang Puno", "parabula_tao_puno", 2));
        parabula.add(new Story("Ang Tunay na Yaman", "parabula_yaman", 3));
        parabula.add(new Story("Ang Usurera", "parabula_usurera", 2));

        map.put("parabula", parabula);

        reader = new XmlReader();
        try {
            root = reader.parse(Gdx.files.internal(Constants.DATA_QUESTIONS));
        } catch (IOException e) {
            Gdx.app.exit();
        }
    }

    public Array<Question> getQuestions(String story){
        Array<Question> questions = new Array<Question>();

        XmlReader.Element storyRoot = root.getChildByName(story);
        Array<XmlReader.Element> questionElems = storyRoot.getChildrenByName("item");

        for(XmlReader.Element questionElem : questionElems){
            Question question = new Question(questionElem.getChildByName("q").getText());

            for(XmlReader.Element c : questionElem.getChildrenByName("c")){
                question.add(new Choice(c.getText(), c.getBooleanAttribute("tama", false)));
            }

            questions.add(question);
        }

        return questions;
    }

    public Array<Story> get(String key){
        Array<Story> result = null;

        if(map.containsKey(key)){
            result = map.get(key);
        }

        return result;
    }

    public class Story{

        private String title;
        private String atlas;
        private int count;

        public Story(String title, String atlas, int count) {
            this.title = title;
            this.atlas = atlas;
            this.count = count;
        }

        public String getTitle() {
            return title;
        }

        public String getAtlas() {
            return atlas;
        }

        public int getCount() {
            return count;
        }
    }

    public class Question{

        private String question;
        private Array<Choice> choices;

        public Question(String question) {
            this.question = question;
            choices = new Array<Choice>();
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public Array<Choice> getChoices() {
            return choices;
        }

        public void add(Choice choice){
            choices.add(choice);
        }
    }

    public class Choice{

        private String choice;
        private boolean correct;

        public Choice(String choice, boolean correct) {
            this.choice = choice;
            this.correct = correct;
        }

        public String getChoice() {
            return choice;
        }

        public boolean isCorrect() {
            return correct;
        }
    }
}
