package com.isu.filipinofolktales.Util;

public class Constants {

    public static final float VIEWPORT_HEIGHT = 1280f;
    public static final float VIEWPORT_WIDTH = 720f;

    public static final String ATLAS_LOADING = "data/ui/loading.jpg";
    public static final String ATLAS_STAGE = "data/ui/atlas_stage.atlas";
    public static final String ATLAS_NOROTATE = "data/ui/atlas_norotate.atlas";

    public static final String SKIN_STAGE = "data/ui/skin_stage.json";
    public static final String SKIN_NOROTATE = "data/ui/skin_norotate.json";

    public static final String AUDIO_CLICK = "data/audio/click.wav";
    public static final String MUSIC_BGM = "data/audio/bgm.mp3";

    public static final String DATA_QUESTIONS = "data/data/questions.xml";
}