package com.isu.filipinofolktales.Util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Assets {

    public static final Assets instance = new Assets();
    private AssetManager assetManager;

    public Skins skins;
    public Audio audio;

    private Assets() {
    }

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;
        startLoading();
    }

    private void startLoading() {
        assetManager.load(Constants.ATLAS_STAGE, TextureAtlas.class);
        assetManager.load(Constants.SKIN_STAGE, Skin.class, new SkinLoader.SkinParameter(Constants.ATLAS_STAGE));

        assetManager.load(Constants.ATLAS_NOROTATE, TextureAtlas.class);
        assetManager.load(Constants.SKIN_NOROTATE, Skin.class, new SkinLoader.SkinParameter(Constants.ATLAS_NOROTATE));

        assetManager.load(Constants.AUDIO_CLICK, Sound.class);
        assetManager.load(Constants.MUSIC_BGM, Music.class);
    }

    public void createClasses(){
        skins = new Skins();
        audio = new Audio();
    }

    public boolean isLoading() {
        return assetManager.update();
    }

    public class Skins{

        public final Skin stage, norotate;

        public Skins() {
            this.stage = assetManager.get(Constants.SKIN_STAGE, Skin.class);
            this.norotate = assetManager.get(Constants.SKIN_NOROTATE, Skin.class);
        }
    }

    public class Audio{

        public final Sound click;
        public final Music bgm;

        public Audio() {
            this.click = assetManager.get(Constants.AUDIO_CLICK, Sound.class);
            this.bgm = assetManager.get(Constants.MUSIC_BGM, Music.class);
        }
    }

    public void dispose() {
        assetManager.dispose();
    }
}