package com.isu.filipinofolktales.Util;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioManager {

    public static final AudioManager audio = new AudioManager();

    private Music playingMusic;

    private AudioManager() {}

    public void playBGM(){
        play(Assets.instance.audio.bgm);
    }

    public void playClick() {
        play(Assets.instance.audio.click);
    }

    public void play(Sound sound) {
        play(sound, 1);
    }

    public void play(Sound sound, float volume) {
        play(sound, volume, 1);
    }

    public void play(Sound sound, float volume, float pitch) {
        play(sound, volume, pitch, 0);
    }

    public void play(Sound sound, float volume, float pitch, float pan) {
        if (!GameSettings.settings.isSound()) return;

        sound.play(GameSettings.settings.getVolume(), pitch, pan);
    }

    public void play(Music music) {
        stopMusic();
        playingMusic = music;
        if (GameSettings.settings.isSound()) {
            music.setLooping(true);
            music.setVolume(GameSettings.settings.getVolume());
            music.play();
        }
    }

    public void stopMusic() {
        if (playingMusic != null) playingMusic.stop();
    }

    public void updateVolume() {
        if (playingMusic != null) {
            playingMusic.setVolume(GameSettings.settings.getVolume());
        }
    }
}
